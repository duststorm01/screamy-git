/*
Copyright (c) 2014, Dragon Avionics, LLC
All rights reserved.

This work is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
See http://creativecommons.org/licenses/by-sa/3.0/ for full license text.
*/

#include <avr/sleep.h>
#include <avr/wdt.h>


/* Constants */

//The different triggers that have been implemented can be individually enabled
const char timer_enabled = 1; //This is an internal timer. Will alarm automatically
const char relay_enabled = 1; //NOTE: Relay is active-low
const char adc_enabled = 1; //System battery monitor
const char servo_enabled = 1; //PWM monitor (quiet when pulse width is between 1500us and 2000us, alarms otherwise)
const char sbus_enabled = 1; //Futaba serial bus (S BUS) monitor. Looking for loss of link bit.

//when does the timer elapse
const unsigned int ALARM_TIMER_SECONDS = 60*60; //1 hour

//at what battery capacity to throw the alarm
const float LOW_BATTERY_ALARM_PERCENTAGE = 0.25; //25%

//The two resistors in the voltage divider are 11.5k & 95.3k
//The sensed voltage is 0.1077 of the input (calculated)
//However, the input pin on the Atmel has some internal resistance, so the actual value is closer to 0.1068 (measured)
const float VOLTAGE_DIVIDER = 0.1068;

//Voltage is scaled into 256 steps between 0V and 2.56V (internal reference)
//This can measure the voltage range 0V to 23.97V in 0.0936V increments.
//A 6S battery at its peak is 25.2V, but is 22.2V nominally
//It is 20.7V at 25%, so although we cannot tell between a full 6S battery and a half-full one, we can tell when a 6S battery is 25%.
const float ADC_STEP = 2.56/256;

const float LOW_BATTERY_1S = 3.2+LOW_BATTERY_ALARM_PERCENTAGE; //used in triggering alarm
const float LOW_BATTERY_1S_STEPS = LOW_BATTERY_1S*VOLTAGE_DIVIDER/ADC_STEP;

//Look up table for determining when to alarm the battery
const unsigned char adc_thresholds[7] = {0,
  (unsigned char) LOW_BATTERY_1S_STEPS,
  (unsigned char) 2*LOW_BATTERY_1S_STEPS,
  (unsigned char) 3*LOW_BATTERY_1S_STEPS,
  (unsigned char) 4*LOW_BATTERY_1S_STEPS,
  (unsigned char) 5*LOW_BATTERY_1S_STEPS,
  (unsigned char) 6*LOW_BATTERY_1S_STEPS};

unsigned char selected_adc_threshold = 0; //0 mean no battery present or disabled
unsigned char max_adc_value = 0;

//Pin definitions. These are more for reference than for use,
//as it's a lot faster (less energy!) to write registers directly than to call digitalWrite()
const char tx_monitor_pin = 2;//PB2, pin 7
const char adc_sys_pin = A3; //ADC3, pin 2
const char buzzer_power_pin = 4; //PB4, pin 3
const char relay_pin = 0; //PB0, pin 5
const char buzzer_pin = 1; //PB1, pin 6

const int NUMBER_OF_ITERATIONS_TO_BYTE23_BIT5 = 330;


/* Variables */
unsigned int alarm_timer = 0;
volatile char pin_change_woke_us_up = 0;
volatile unsigned char timeout = 0;

enum system_type_e { UNKNOWN, SBUS, PWM_TRIG }; 
system_type_e mySystem = UNKNOWN;

/* Interrupt Service Routines */

//This runs each time the watch dog wakes us up from sleep
ISR(WDT_vect) {
  //Don't do anything. This is just here so that we wake up.
}

//Sets a flag so a while loop has a backup exit condition
ISR(TIMER1_COMPA_vect)
{
  timeout = 1;
}

ISR(TIMER0_COMPA_vect)
{
  //Here to wake us up when the timer elapses
}

//This runs when we are monitoring the servo input pin TX_MONITOR and it changes
ISR(PCINT0_vect) {
    pin_change_woke_us_up = 1;
}


void setup()
{
    //ADC configuration
    //Power down ADC
    ADCSRA = 0; //ADC disabled to save power, div 2 clock prescaler
    ADCSRB = 0; //free running / single conversion mode
    DIDR0 |= 1<<ADC3D; //disable digital input on the pin we're using for ADC
    ADMUX = (1<<REFS2) | (1<<REFS1); //use internal 2.56V reference w/o external bypass capacitor, disconnected from PB0/AREF
    ADMUX |= 1<<ADLAR; //left-adjust to just read the high 8 bits from ADCH
    ADMUX |= 0x3; //ADC channel 3    
    
    //determine direction of pins
    PORTB = (1<<PB0)|(1<<PB2); //internal pullups on relay pin and TX monitoring pin
    DDRB = (1<<PB1)|(1<<PB4); //outputs are buzzer pin and buzzer power pin, everything else is an input
    
    //beep once on power up
    alarm();
}


void loop()
{   
    //NOTE: You can change how often Screamy beeps by editing the next setup_watchdog()
    //instruction. Intervals start at 16ms and double up to a maximum of 8 seconds
    //Factory default is 4 seconds
    //If you change the interval, be sure to change the alarm timer increment to match
    
    setup_watchdog(8); //Setup watchdog to go off after 4sec
    //Power down various bits of hardware to lower power usage
    set_sleep_mode(SLEEP_MODE_PWR_DOWN); //Power down everything, wake up from WDT
    sleep_enable();
    sleep_mode(); //Go to sleep! Wake up 4sec later
    sleep_disable();

    //increment alarm timer
    if (alarm_timer < ALARM_TIMER_SECONDS)
      alarm_timer += 4; //4 seconds
   
    //check for triggering conditions
    if (relayTrigger())
        alarm();
    else if (timerTrigger(alarm_timer))
        alarm();
    else if (lowSystemBatteryTrigger())
        alarm();
    else if (sbusTrigger() && invalidPWMTrigger())
        alarm();
}


//Reference: https://github.com/sparkfun/H2OhNo/blob/master/firmware/H2OhNo/H2OhNo.ino

//Sets the watchdog timer to wake us up, but not reset
//0=16ms, 1=32ms, 2=64ms, 3=128ms, 4=250ms, 5=500ms
//6=1sec, 7=2sec, 8=4sec, 9=8sec
//From: http://interface.khm.de/index.php/lab/experiments/sleep_watchdog_battery/
void setup_watchdog(int timerPrescaler) {

  if (timerPrescaler > 9 ) timerPrescaler = 9; //Limit incoming amount to legal settings

  char bb = timerPrescaler & 7;
  if (timerPrescaler > 7) bb |= (1<<5); //Set the special 5th bit if necessary

  //This order of commands is important and cannot be combined
  MCUSR &= ~(1<<WDRF); //Clear the watch dog reset
  WDTCR |= (1<<WDCE) | (1<<WDE); //Set WD_change enable, set WD enable
  WDTCR = bb; //Set new watchdog timeout value
  WDTCR |= _BV(WDIE); //Set the interrupt enable, this will keep unit from resetting after each int
}

//Function for alarming on relay
char relayTrigger()
{
    return relay_enabled && !(PINB & (1<<PINB0)); //relay is active-low
}

//Function for alarming when the timer elapses
char timerTrigger(int timer)
{
    return timer_enabled && (timer >= ALARM_TIMER_SECONDS);
}

//Function for alarming when the battery runs low
char lowSystemBatteryTrigger()
{
    if (!adc_enabled)
        return 0;

    ADCSRA = (1<<ADEN); //Enable ADC
   
    ADCSRA |= 1<<ADSC; //start conversion

    while (ADCSRA & (1<<ADSC)) ; //when the bit is cleared, the ADC is finished

    int result = ADCH;

    ADCSRA &= ~(1<<ADEN); //Disable ADC, saves ~230uA

    if (result > max_adc_value)
    {
       //voltage is rising, indicating a battery is being connected
       max_adc_value = result;
      
       for (unsigned char i = 1; i < 7; ++i)
       {
         if (max_adc_value < adc_thresholds[i])
         {
             selected_adc_threshold = i-1;
             return 0;
         }
       }       
       
       selected_adc_threshold = 6;
       return 0;

    }
    else 
      return (result < adc_thresholds[selected_adc_threshold]);

    return 0;
}



char invalidPWMTrigger()
{
    if (!servo_enabled)
        return 0;

    //valid PWM:
    //up at time=0 (timeout at 32ms)
    //down at time>900us && time<2100us
    //to give users the ability to trigger the alarm without shutting off the transmitter,
    //we will alarm if the PWM value is less than 1500us

    //set watchdog timer for 32ms
    //PWM signal repeats every 20ms, so 32ms is a comfortable margin
    setup_watchdog(1);

    //set interrupt for rising edge
    //TX_MONITOR is PCINT2
    GIMSK = 1<<PCIE; //set pin change interrupt enable in general interrupt mask register
    PCMSK = 1<<PCINT2; // enable pin 2 for interrupt
    sei(); //enable global interrupts

    //loop until we find a rising edge
    char rising_edge = 0;
    while (!rising_edge)
    {
        //wait
        set_sleep_mode(SLEEP_MODE_PWR_DOWN); // sleep mode is set here
        sleep_enable();

        sleep_mode();                        // System sleeps here

        sleep_disable();                     // System continues execution here when watchdog timed out 

        //what woke us up? watchdog? or pin change?
        if (!pin_change_woke_us_up) //watchdog timer
            return returnPWM(1);

        //pin change woke us up, clear flag
        pin_change_woke_us_up = 0;

        //what state is the pin? rising, or falling?
        rising_edge = PINB & (1<<PINB2);
    }


    //now that we have found the rising edge, measure the pulse width
    //interrupt on the next pin change, which will be a falling edge
    //set a timeout so we can report an invalid PWM

    //set watchdog timer for 16ms
    setup_watchdog(0);

    //save off current time
    //timer1 and timer0 are 8-bit timers
    //assuming 1MHz clock, there are 2000 clock cycles in 2ms, 31.25 with a div 64 prescaler (64us steps)
    GTCCR = 1 << PSR0; //reset prescaler free-running counter
    TCCR0A = 0; //normal timer mode (no CTC or PWM) 
    TCCR0B = 0x3; //div64 prescaler to give 64us steps to timer
    TCNT0 = 0; //reset the timer

    char timestamp1 = TCNT0;


    //wait for falling edge
    set_sleep_mode(SLEEP_MODE_IDLE); // need idle and not power down because we need the counter to measure time
    sleep_enable();

    sleep_mode();                        // System sleeps here

    sleep_disable();                     // System continues execution here when watchdog timed out 

    //save off current time
    char timestamp2 = TCNT0;

    //what woke us up? watchdog? or pin change?
    if (!pin_change_woke_us_up) //watchdog timer
        return returnPWM(1);

    //pin change woke us up, clear flag
    pin_change_woke_us_up = 0;

    //measure pulse width
    char diff = timestamp2-timestamp1;

    //validate pulse width
    char valid = diff > 34 || diff < 23; //34=2.176ms, 23=1.472ms
    
    return returnPWM(valid); 
}

//clean up helper function for invalidPWMTrigger
char returnPWM(char val)
{
    GIMSK &= ~(1<<PCIE); //clear pin change interrupt enable in general interrupt mask register
    PCMSK &= ~(1<<PCINT2); // disable pin 2 for interrupt

    return val;
}

char sbusTrigger()
{
    //looking for a particular bit in the messsage or a timeout
    if (!sbus_enabled)
        return 0;


    //STEP 1: Look for idle condition inbetween messages
    //The message is 3ms long and is sent at least once every 14ms
    //32 ms is enough for a watchdog timer
   
    //set timer for 32ms
    timeout = 0;
    TCCR1 = 0x8; //counter to run at CK/128
    TCNT1 = 0;
    OCR1A = 250; //250 @ 1MHz/128 = 32ms
    TIMSK = 0x40; //OCIE1A: counter1 interrupt enable
    sei();

    //Look for 16 straight zeros
    int input_counter = 0;
      
    while (!timeout && input_counter < 16)
    {
      if (PINB & (1<<PB2))
         input_counter = 0;
      else
          ++input_counter;
    }
    
    //stop timer
    TCCR1 = 0;
    TIMSK = 0;

    //watchdog timer elapsed -- bus activity resembles noise instead of a serial protocol
    //sound alarm
    if (timeout)
    {
      return 1;
    }    
    
    //we now have 16 straight zeros

    //STEP 2: Look for rising edge (start bit)
    pin_change_woke_us_up = 0;

    GIMSK = 1<<PCIE; //set pin change interrupt enable in general interrupt mask register
    PCMSK = 1<<PCINT2; // enable pin 2 for interrupt
    sei();

    setup_watchdog(1);

    set_sleep_mode(SLEEP_MODE_IDLE); //power down takes too long to wake up from
    sleep_enable();

    sleep_mode();                        // System sleeps here

    sleep_disable();                     // System continues execution here when watchdog timed out 

    GIMSK &= ~(1<<PCIE); //clear pin change interrupt enable in general interrupt mask register
    PCMSK &= ~(1<<PCINT2); // disable pin 2 for interrupt

    //what woke us up? watchdog? or pin change?
    if (!pin_change_woke_us_up) //watchdog timer
    {      
        //bus is silent, sound alarm
        return 1;
    }

    //STEP 3: Go to byte 23 bit 5
    //Using a for loop here instead of an interrupt for better cycle accuracy
    //There is still some jitter, so the center of the bit is targeted
    //This counter value was empirically determined
    //const int NUMBER_OF_ITERATIONS_TO_BYTE23_BIT5 = 330;
    int count;
    for (count = 0; count < NUMBER_OF_ITERATIONS_TO_BYTE23_BIT5; ++count) 
    {
      asm("NOP");
    };

    char ret = PINB;
    ret = ret & (1<<PB2); //mask: only look at tx monitor bit
     
    //STEP 4: if it's a 0, alarm 
    return !ret;
}



//This is it! Sound the alarm!
void alarm()
{
    //enable buzzer power
    PORTB |= 1<<PB4;
    
    //buzzer stuff
    TCCR0A = 0x12; //COM0B = 2'b01, WGM = 3'b010
    TCCR0B = 1; //no clock prescaling
    OCR0B = 1;
    OCR0A = 125; //125 cycles @ 1MHz = 8 kHz. Toggling halves this to a 4kHz
                 //square wave
    
    //idle the CPU for half a second. The timers will keep going and the buzzer will sound.
    setup_watchdog(5);
    set_sleep_mode(SLEEP_MODE_IDLE); // sleep mode is set here
    sleep_enable();
    sleep_mode();                        // System sleeps here
    sleep_disable();                     // System continues execution here when watchdog timed out 

    //turn off buzzer
    TCCR0A = 0;
    TCCR0B = 0;

    //disable buzzer power
    PORTB &= ~(1<<PB4);
}

